package structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeHour {
	
	public List<StructureObject> objects;
	public Map<String, StructureObject> objectsByKey;
	public Integer hour;
	
	public List<StructureObject> orphanObjectComps;
	
	public TreeHour(Integer hour) {
		this.hour = hour;
		objects = new ArrayList<StructureObject>();
		objectsByKey = new HashMap<String, StructureObject>();
		
		orphanObjectComps = new ArrayList<StructureObject>();
	}
	
	public void addObject(StructureObject so) {

		if (so.storageType.equals("base")) {
			return;
		}
		
		objects.add(so);
		objectsByKey.put(so.id, so);
		
	}
	
	public void addObjectComp(StructureObject so) {
		StructureObject thisObject = objectsByKey.get(so.id);
		
		if (thisObject == null) {
			orphanObjectComps.add(so);
		} else {
			thisObject.hasCompactedMarker = true;
		}
	}
	
	
	// -----------
	// Computation
	// -----------
	
	public Integer getTotalFiles() {
		return objects.size();
	}
	
	public Integer getTotalNoncompactedFiles() {
		int i = 0;
		for (StructureObject so :objects) {
			if (!so.hasCompactedMarker) {
				i++;
			}
		}
		return i;
	}
	
	public Integer getTotalCompactedFiles() {
		int i = 0;
		for (StructureObject so :objects) {
			if (so.hasCompactedMarker) {
				i++;
			}
		}
		return i;
	}
	
	public Integer getTotalOrphanComps() {
		int i = 0;
		i += orphanObjectComps.size();
		return i;
	}
	
	// ----------
	
	public Integer getTotalNoncompactedFileSize() {
		int i = 0;
		for (StructureObject so :objects) {
			if (!so.hasCompactedMarker) {
				i += so.fileSize;
			}
		}
		return i;
	}
	
	public Integer getTotalCompactedFileSize() {
		int i = 0;
		for (StructureObject so :objects) {
			if (so.hasCompactedMarker) {
				i += so.fileSize;
			}
		}
		return i;
	}
	
	
	// ---------
	// Utilities
	// ---------
	
	public void sort() {
		objects.sort(new StructureObjectTimestampComparator());
	}

}
