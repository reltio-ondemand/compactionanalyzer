package structure;

import java.util.Comparator;

public class TreeObjectTypeComparator implements Comparator<TreeObjectType> {
	
	public int compare(TreeObjectType tot1, TreeObjectType tot2) {
		return tot1.objectType.compareTo(tot2.objectType);
	}
	
}
