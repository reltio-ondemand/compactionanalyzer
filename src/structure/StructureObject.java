package structure;
import java.sql.Timestamp;
import java.util.Comparator;

public class StructureObject {

	// Keys:
	//   storageType
	//     objectSuperType
	//       objectType
	//         timestamp
	//         id
	
	
	// Keys
	public String storageType; // storageType key  ++
	public String objectSuperType; // objectSuperType key
	public String objectType; // objectType key
	public Integer hourOrBaseNumber; // hour key  ++    (set to 0 for main storage)
	

	// Keys - Main Storage
	Integer updateNumber;
	
	// Keys - Updates Queue
	public String id; // structureObject key2
	
	
	// All Types
	public String path;
	public Boolean isFolderMarker;
	public Boolean isCompactedMarker;
	public Boolean marksFolder; // folder marker OR compacted marker that points to folder
	public Timestamp timestamp; // structureObject key1
	public Boolean omit;
	public Boolean hasCompactedMarker; // not used by compacted markers, but by files themselves
	public Timestamp createTime; 
	public Integer fileSize;
	
	
	
	
	public StructureObject(String pathWithDetails) {
		hasCompactedMarker = false;
		String trimmed = pathWithDetails.replaceAll("\\s+"," ");
		String[] parts = trimmed.split(" ");
		marksFolder = false;
		
		
		//createTime = Timestamp.valueOf(parts[0]);
		//System.out.println(parts[0]);
		//System.out.println(parts[1]);
		//System.out.println(parts[2]);
		//System.out.println(parts[3]);
		fileSize = Integer.valueOf(parts[2]);
		path = parts[3];
		
		isFolderMarker = path.endsWith("$folder$");
		isCompactedMarker = path.endsWith("_compacted");
		
		String[] pathParts = path.split("/");
		
		String[] subParts = "".split(" ");
		
		
		
		// Omits
		if (pathParts[0].equals("updates.queue_$folder$")) {
			omit = true;
			return;
		}
		
		if (pathParts[0].equals("updates.queue")) {
			// Start processing updates queue
			
			if (pathParts[1].equals("staging_$folder$")) {
				omit = true;
				return;
			}
			
			
			if (pathParts[1].equals("staging")) {
				storageType = "staging";
				if (isCompactedMarker || isFolderMarker) {
					int i = pathParts[2].indexOf("_");
					if (i > 0) {
						hourOrBaseNumber = Integer.valueOf(pathParts[2].substring(0, i));
					} else {
						hourOrBaseNumber = Integer.valueOf(pathParts[2]);
						subParts = pathParts[3].split("_");
					}
				} else {
					hourOrBaseNumber = Integer.valueOf(pathParts[2]);
					subParts = pathParts[3].split("_");
				}
				
			} else {
				storageType = "main";
				if (isCompactedMarker || isFolderMarker) {
					int i = pathParts[1].indexOf("_");
					if (i > 0) {
						hourOrBaseNumber = Integer.valueOf(pathParts[1].substring(0, i));
					} else {
						hourOrBaseNumber = Integer.valueOf(pathParts[1]);
						subParts = pathParts[2].split("_");
					}
				} else {
					hourOrBaseNumber = Integer.valueOf(pathParts[1]);
					subParts = pathParts[2].split("_");
				}
				
				/*if (isFolderMarker) {
					// Do nothing...
				} else if (isCompactedMarker) {
					// todo...
					// Need to consider if marker belongs to a file.
				} else {
					id = subParts[0];
					objectSuperType = subParts[1];
					objectType = subParts[2];
					timestamp = Timestamp.valueOf(subParts[3]);
				}*/
				
			}
			
			if (isFolderMarker) {
				// TODO
				// Can get:
				//   +storageType
				//   +hour
				// Can sometimes get:
				//   -objectSuperType
				//   -objectType
				//   -baseNumber
				//   -updateNumber
				
				marksFolder = true;
			} else if (isCompactedMarker) {
				// TODO
				// Can get:
				//   +storageType
				//   +hour
				// Can sometimes get:
				//   -objectSuperType
				//   -objectType
				//   -timestamp
				//   -id
				// Need to consider if marker belongs to a file.
				
				if (subParts.length == 5) {
					marksFolder = false;
					objectSuperType = subParts[1];
					id = subParts[0];
					objectType = subParts[2];
				} else {
					marksFolder = true;
				}
			} else {
				id = subParts[0];
				objectSuperType = subParts[1];
				objectType = subParts[2];
				//timestamp = Timestamp.valueOf(subParts[3]);
			}
		} else {
			// Start processing main storage
			objectSuperType = pathParts[0];
			objectType = pathParts[1];
			
			subParts = pathParts[2].replaceAll("_", "-").split("-");
			hourOrBaseNumber = Integer.valueOf(subParts[1]);
			if (subParts[2].equals("full")) {
				storageType = "base";
				updateNumber = 0;
				// id intentionally null
			} else {
				storageType = "update";
				updateNumber = Integer.valueOf(subParts[3]);
				id = subParts[3];
			}
			
			
		}
		// Omit logic:
		// If non-updates queue, omit all non-markers
		// TODO: For now, filter these by regex (or even, filter be regex before this portion of code?)
		omit = false;
			
	}
	
}
