package structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeObjectSuperType {

	public List<TreeObjectType> objects;
	public Map<String, TreeObjectType> objectsByKey;
	public String objectSuperType;
	
	public List<StructureObject> orphanObjectComps;
	
	public TreeObjectSuperType(String objectType) {
		this.objectSuperType = objectType;
		objects = new ArrayList<TreeObjectType>();
		objectsByKey = new HashMap<String, TreeObjectType>();
		
		orphanObjectComps = new ArrayList<StructureObject>();
	}
	
	public void addObject(StructureObject so) {
		// 1. Check if TreeHour exists
		String objectType = so.objectType;
		
		TreeObjectType treeObjectType = objectsByKey.get(objectType);
		
		if (treeObjectType == null) {
			treeObjectType = new TreeObjectType(objectType);
			objectsByKey.put(objectType, treeObjectType);
			objects.add(treeObjectType);
		}
		
		treeObjectType.addObject(so);
	}
	
	public void addObjectComp(StructureObject so) {
		String objectType = so.objectType;
		
		TreeObjectType treeObjectType = objectsByKey.get(objectType);
		
		if (treeObjectType == null) {
			orphanObjectComps.add(so);
		} else {
			treeObjectType.addObjectComp(so);
		}
	}
	
	public void sort() {
		objects.sort(new TreeObjectTypeComparator());
	}
	
	public void deepSort() {
		sort();
		for (TreeObjectType th :objects) {
			th.deepSort();
		}
	}

}
