package structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeRoot {
	
	List<TreeStorageType> objects;
	Map<String, TreeStorageType> objectsByKey;
	
	List<StructureObject> orphanObjectComps;
	
	public TreeRoot() {
		objects = new ArrayList<TreeStorageType>();
		objectsByKey = new HashMap<String, TreeStorageType>();
		
		orphanObjectComps = new ArrayList<StructureObject>();
	}
	
	public void addObject(StructureObject so) {
		// 1. Check if TreeHour exists
		String objectStorageType = so.storageType;
		
		if (objectStorageType.equals("base")) {
			objectStorageType = "update";
		}
		
		TreeStorageType treeStorageType = objectsByKey.get(objectStorageType);
		
		if (treeStorageType == null) {
			treeStorageType = new TreeStorageType(objectStorageType);
			objectsByKey.put(objectStorageType, treeStorageType);
			objects.add(treeStorageType);
		}
		
		treeStorageType.addObject(so);
	}
	
	public void addObjectComp(StructureObject so) {
		
		String objectStorageType = so.storageType;
		
		TreeStorageType treeStorageType = objectsByKey.get(objectStorageType);
		
		if (treeStorageType == null) {
			orphanObjectComps.add(so);
		} else {
			treeStorageType.addObjectComp(so);
		}
	}
	
	public TreeStorageType getByType(String type) {
		return objectsByKey.get(type);
	}
	
	
	
	
	public void sort() {
		// TODO...
	}
	
	public void deepSort() {
		sort();
		for (TreeStorageType tst :objects) {
			tst.deepSort();
		}
	}


}
