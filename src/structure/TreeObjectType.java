package structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeObjectType {

	public List<TreeHour> objects;
	public Map<Integer, TreeHour> objectsByKey;
	public String objectType;
	
	public List<StructureObject> orphanObjectComps;
	
	public TreeObjectType(String objectType) {
		this.objectType = objectType;
		objects = new ArrayList<TreeHour>();
		objectsByKey = new HashMap<Integer, TreeHour>();
		
		orphanObjectComps = new ArrayList<StructureObject>();
	}
	
	// ---------
	// Accessors
	// ---------
	
	public void addObject(StructureObject so) {
		// 1. Check if TreeHour exists
		Integer hour = so.hourOrBaseNumber;
		
		TreeHour treeHour = objectsByKey.get(hour);
		
		if (treeHour == null) {
			treeHour = new TreeHour(hour);
			objectsByKey.put(hour, treeHour);
			objects.add(treeHour);
		}
		
		treeHour.addObject(so);
	}
	
	public void addObjectComp(StructureObject so) {
		// 1. Check if TreeHour exists
		Integer hour = so.hourOrBaseNumber;
		
		TreeHour treeHour = objectsByKey.get(hour);
		
		if (treeHour == null) {
			orphanObjectComps.add(so);
		} else {
			treeHour.addObjectComp(so);
		}
	}
	
	
	
	// -----------
	// Computation
	// -----------
	
	public Integer getTotalFiles() {
		int i = 0;
		for (TreeHour th :objects) {
			i += th.getTotalFiles();
		}
		return i;
	}
	
	public Integer getTotalNoncompactedFiles() {
		int i = 0;
		for (TreeHour th :objects) {
			i += th.getTotalNoncompactedFiles();
		}
		return i;
	}
	
	public Integer getTotalCompactedFiles() {
		int i = 0;
		for (TreeHour th :objects) {
			i += th.getTotalCompactedFiles();
		}
		return i;
	}
	
	public Integer getTotalOrphanComps() {
		int i = 0;
		for (TreeHour th :objects) {
			i += th.getTotalOrphanComps();
		}
		i += orphanObjectComps.size();
		return i;
	}
	
	// ----------
	
	public Integer getTotalNoncompactedFileSize() {
		int i = 0;
		for (TreeHour th :objects) {
			th.getTotalNoncompactedFileSize();
		}
		return i;
	}
	
	public Integer getTotalCompactedFileSize() {
		int i = 0;
		for (TreeHour th :objects) {
			i += th.getTotalCompactedFileSize();
		}
		return i;
	}
	
	
	
	
	// -----------------------
	// Nonfunctional Utilities
	// -----------------------
	
	public void sort() {
		objects.sort(new TreeHourComparator());
	}
	
	public void deepSort() {
		sort();
		for (TreeHour th :objects) {
			th.sort();
		}
	}
	
}
