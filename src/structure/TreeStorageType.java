package structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeStorageType {

	public List<TreeObjectSuperType> objects;
	public Map<String, TreeObjectSuperType> objectsByKey;
	public String storageType;
	
	public List<StructureObject> orphanObjectComps;
	
	public TreeStorageType(String storageType) {
		this.storageType = storageType;
		objects = new ArrayList<TreeObjectSuperType>();
		objectsByKey = new HashMap<String, TreeObjectSuperType>();
		
		orphanObjectComps = new ArrayList<StructureObject>();
	}
	
	public void addObject(StructureObject so) {
		// 1. Check if TreeHour exists
		String objectSuperType = so.objectSuperType;
		
		TreeObjectSuperType treeObjectSuperType = objectsByKey.get(objectSuperType);
		
		if (treeObjectSuperType == null) {
			treeObjectSuperType = new TreeObjectSuperType(objectSuperType);
			objectsByKey.put(objectSuperType, treeObjectSuperType);
			objects.add(treeObjectSuperType);
		}
		
		treeObjectSuperType.addObject(so);
	}
	
	public void addObjectComp(StructureObject so) {
		
		String objectSuperType = so.objectSuperType;
		
		TreeObjectSuperType treeObjectSuperType = objectsByKey.get(objectSuperType);
		
		if (treeObjectSuperType == null) {
			orphanObjectComps.add(so);
		} else {
			treeObjectSuperType.addObjectComp(so);
		}
	}
	
	public void sort() {
		// TODO...
	}
	
	public void deepSort() {
		sort();
		for (TreeObjectSuperType tost :objects) {
			tost.deepSort();
		}
	}

}
