package structure;

import java.util.Comparator;

public class TreeHourComparator implements Comparator<TreeHour> {
	
	public int compare(TreeHour th1, TreeHour th2) {
		if (th1.hour > th2.hour) {
			return 1;
		} else if (th1.hour < th2.hour) {
			return -1;
		}
		
		return 0;
	}

}
