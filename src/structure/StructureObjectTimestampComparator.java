package structure;

import java.util.Comparator;

public class StructureObjectTimestampComparator implements Comparator<StructureObject> {
	
	public int compare(StructureObject so1, StructureObject so2) {
		if (so1.timestamp.after(so2.timestamp)) {
			return 1;
		} else if (so1.timestamp.before(so2.timestamp)) {
			return -1;
		}
		
		return 0;
	}

}
