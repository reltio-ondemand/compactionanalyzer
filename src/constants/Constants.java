package constants;

public class Constants {

	public static final String VERSION = "0.0.1";
	public static final Integer DYNAMIC_COMPACTION_SIZE = 10000;
	public static final Integer SHALLOW_COMPACTION_ELIGIBLE_PROXY = 25;
	public static final Integer SHALLOW_COMPACTION_ELIGIBLE_DIRECT = 50;
	public static final Integer DEEP_COMPACTION_ELIGIBLE = 12;
	
}
