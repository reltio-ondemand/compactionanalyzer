import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import constants.Constants;
import structure.StructureObject;
import structure.TreeHour;
import structure.TreeObjectSuperType;
import structure.TreeObjectType;
import structure.TreeRoot;
import structure.TreeStorageType;

/*
 *  Program can be run on listings from RIQ S3 bucket.
 *  Supported usage:
 *    entityTypes, relationTypes, updatesQueue (entities, relations)
 *  Recommended usage:
 *    Provide recursive listings of 
 *    
 *  Updates Queue Scenarios
 *    1. Stray folder marker
 *    2. Non-compacted with size
 *    3. Compacted with size
 *         - Still visible in Delta DA
 *    4. Stray compacted marker (?)
 *    5. 
 */

public class CompactionAnalyser {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		String filePath = args[0];
		// String riqConfigPath = args[1];
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
		
		
		List<StructureObject> allObjects = new ArrayList<StructureObject>();
		
		String next = bufferedReader.readLine();
		while (next != null) {
			System.out.println("Process line: " + next);
			StructureObject so = new StructureObject(next);
			if (so.omit) {
				// do nothing...
			} else {
				allObjects.add(so);
			}
			next = bufferedReader.readLine();
		}
		
		// Sort into files, compaction markers, and folder markers
		List<StructureObject> objectFiles = new ArrayList<StructureObject>();
		List<StructureObject> objectFolders = new ArrayList<StructureObject>(); // TODO
		List<StructureObject> folderCompMarks = new ArrayList<StructureObject>(); // TODO
		List<StructureObject> objectCompMarks = new ArrayList<StructureObject>();
		
		int i = 0;
		for (StructureObject so :allObjects) {
			
			i++;
			if (i == 3414) {
				System.out.println("Stop here...");
			}
			
			System.out.println("" + i + ": " + so.path);;
			
			if (so.isCompactedMarker && so.marksFolder) {
				folderCompMarks.add(so);
			} else if (so.isCompactedMarker && !so.marksFolder) {
				objectCompMarks.add(so);
			} else if (so.isFolderMarker) {
				objectFolders.add(so);
			} else {
				// Assume to be a file
				objectFiles.add(so);
			}
		}
		
		// Build tree / Logic
		// 1. Build tree from files
		//      - {{storageType}}.{{objectSuper|ixA}}.{{objectType|ixA}}.{{hour|ix#}}.[objects]
		//      - Ex: staging.entity.HCP.20180801.<object>
		// 2. Attempt to match folder markers
		//      - identify "orphan" markers => separate list
		// 3. Attempt to match compacted markers
		//      - identify "orphan" markers => separate list
		
		TreeRoot treeRoot = new TreeRoot();
		
		
		int count = 0;
		for (StructureObject so :objectFiles) {
			treeRoot.addObject(so);
			count++;
		}
		System.out.println("Objects added: " + count);
		
		for (StructureObject so :objectCompMarks) {
			treeRoot.addObjectComp(so);
		}
		
		
		
		
		// Build report / Logic
		// for storageType = updates.staging (DYNAMIC_COMPACTION):
		//   for each objectSuper:
		//     for each objectType:
		//       aggregate (by count and volume):
		//         uncompacted files
		//         compacted files
		//         eligibilities
		//       write output
		// for storageType = updates.main (SHALLOW_COMPACTION):
		//   for each objectSuper:
		//     for each objectType:
		//       aggregate (by count):
		//         uncompacted files
		//         compacted files
		//         compacted files, old
		//         eligibilities
		// for storageTypes update ("DEEP_COMPACTION):
		//   for each objectSuper:
		//     for each objectType:
		//       aggregate (by count):
		//         file markers
		
		// TreeRoot.staging
		TreeStorageType treeStaging = treeRoot.getByType("staging");
		System.out.println("DYNAMIC COMPACTION:");
		for (TreeObjectSuperType tost :treeStaging.objects) {
			System.out.println(".." + tost.objectSuperType);
			for (TreeObjectType tot :tost.objects) {
				System.out.println("...." + tot.objectType);
				System.out.println("......Compacted Files: " + tot.getTotalCompactedFiles());
				System.out.println("......Noncompacted Files: " + tot.getTotalNoncompactedFiles());
				System.out.println("......Orphan Compacted Markers: " + tot.getTotalOrphanComps());
				System.out.println("......Total Compacted File Size: " + tot.getTotalCompactedFileSize());
				System.out.println("......Total Noncompacted File Size: " + tot.getTotalNoncompactedFileSize());
				
				Boolean eligible = false;
				if (tot.getTotalNoncompactedFileSize() > Constants.DYNAMIC_COMPACTION_SIZE) {
					eligible = true;
				}
				System.out.println("......Dynamic Compaction Eligible: " + eligible);
			}
		}
		
		// TreeRoot.main
		TreeStorageType treeMain = treeRoot.getByType("main");
		System.out.println("SHALLOW COMPACTION:");
		for (TreeObjectSuperType tost :treeMain.objects) {
			System.out.println(".." + tost.objectSuperType);
			for (TreeObjectType tot :tost.objects) {
				System.out.println("...." + tot.objectType);
				System.out.println("......Compacted Files: " + tot.getTotalCompactedFiles());
				System.out.println("......Noncompacted Files: " + tot.getTotalNoncompactedFiles());
				System.out.println("......Orphan Compacted Markers: " + tot.getTotalOrphanComps());
				
				Boolean eligibleByProxy = false;
				Boolean eligibleDirect = false;
				if (tot.getTotalNoncompactedFiles() >= Constants.SHALLOW_COMPACTION_ELIGIBLE_PROXY) {
					eligibleByProxy = true;
				}
				if (tot.getTotalNoncompactedFiles() >= Constants.SHALLOW_COMPACTION_ELIGIBLE_DIRECT) {
					eligibleDirect = true;
				}
				System.out.println("......Compaction Eligible by Proxy: " + eligibleByProxy);
				System.out.println("......Compaction Eligible Direct: " + eligibleDirect);
			}
		}
		
		// TreeRoot.update
		TreeStorageType treeUpdate = treeRoot.getByType("update");
		System.out.println("DEEP COMPACTION:");
		for (TreeObjectSuperType tost :treeUpdate.objects) {
			System.out.println(".." + tost.objectSuperType);
			for (TreeObjectType tot :tost.objects) {
				System.out.println("...." + tot.objectType);
				
				Integer maxBase = 0;
				for (TreeHour th :tot.objects) {
					if (th.hour > maxBase) {
						maxBase = th.hour;
					}
				}
				System.out.println("......Latest Base Number: " + maxBase);
				
				Integer updatesCount = tot.objectsByKey.get(maxBase).objects.size();
				System.out.println("......Number of updates to latest base: " + updatesCount);
				
				Boolean eligibleForDeepCompaction = false;
				if (updatesCount >= Constants.DEEP_COMPACTION_ELIGIBLE) {
					eligibleForDeepCompaction = true;
				}
				System.out.println("......Eligible for Deep Compaction: " + eligibleForDeepCompaction);
			}
		}
	}
}
